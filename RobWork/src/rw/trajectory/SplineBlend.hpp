#include <rw/core/macros.hpp>
#include <rw/trajectory/Blend.hpp>
#include <rw/trajectory/Interpolator.hpp>

#include <cmath>
#include <iostream>

namespace rw { namespace trajectory {

    template<class T> class SplineBlend : public rw::trajectory::Blend<T>
    {
      public:
        SplineBlend(const rw::trajectory::Interpolator<T>& interp1,
                    const rw::trajectory::Interpolator<T>& interp2, double tau1, double tau2) :
            _tau1(tau1),
            _tau2(tau2) {
            if(tau1 > interp1.duration() || tau1 < 0) { RW_THROW("tau1 is out of bounds"); }
            if(tau2 > interp2.duration() || tau2 < 0) { RW_THROW("tau2 is out of bounds"); }

            T x0 = interp1.x(interp1.duration() - tau1);
            T x1 = interp2.x(tau2);

            if(x1.size() != x0.size()) { RW_THROW("x0 and x1 must have the same size"); }

            T v0 = interp1.dx(interp1.duration() - tau1);
            T v1 = interp2.dx(tau2);

            _a = x0;
            _b = v0;

            double t1 = tau1 + tau2;

            _d = x0;
            _c = v0;

            for(size_t i = 0; i < x0.size(); i++) {
                _d[i] = (2 * _a[i] + _b[i] * t1 + t1 * v1[i] - 2 * x1[i]) / (t1 * t1 * t1);
                _c[i] = -(3 * _a[i] + 2 * _b[i] * t1 + t1 * v1[i] - 3 * x1[i]) / (t1 * t1);
            }
        }

        /**
         * @brief The position for a given time t
         * @param t [in] \f$ t\in[0,\tau_1+\tau_2] \f$
         * @return Position at time \b t
         */
        virtual T x(double t) const { return _a + _b * t + _c * t * t + _d * t * t * t; }

        /**
         * @brief The velocity for a given time t
         * @param t [in] \f$ t\in[0,\tau_1+\tau_2] \f$
         * @return Velocity at time \b t
         */
        virtual T dx(double t) const { return _b + 2 * _c * t + 3 * _d * t * t; }

        /**
         * @brief The acceleration for a given time t
         * @param t [in] \f$ t\in[0,\tau_1+\tau_2] \f$
         * @return Acceleration at time \b t
         */
        virtual T ddx(double t) const { return 2 * _c + 6 * _d * t; }

        T maxDdx() const {
            T a0 = std::abs(this->ddx(0));
            T a1 = std::abs(this->ddx(_tau1 + _tau2));

            T max = a0;
            for(size_t i = 0; i < a0.size(); i++) {
                if(a1[i] > max[i]) { max[i] = a1[i]; }
            }
            return max;
        }

        /**
         * @brief The time \f$\tau_1\f$ as defined in class definition
         * @return \f$\tau_1\f$
         */
        virtual double tau1() const { return _tau1; }

        /**
         * @brief The time \f$\tau_2\f$ as defined in class definition
         * @return \f$\tau_2\f$
         */
        virtual double tau2() const { return _tau2; }

      private:
        double _tau1;
        double _tau2;
        double _duration;

        T _a;
        T _b;
        T _c;
        T _d;
    };

    template<>
    SplineBlend<double>::SplineBlend(const rw::trajectory::Interpolator<double>& interp1,
                                     const rw::trajectory::Interpolator<double>& interp2,
                                     double tau1, double tau2);
    template<> double SplineBlend<double>::x(double t) const;
    template<> double SplineBlend<double>::dx(double t) const;
    template<> double SplineBlend<double>::ddx(double t) const;
    template<> double SplineBlend<double>::maxDdx() const;

    extern template class SplineBlend<double>;

}}    // namespace rw::trajectory