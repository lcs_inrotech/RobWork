#include <rw/trajectory/SplineBlend.hpp>

namespace rw { namespace trajectory {

    template<>
    SplineBlend<double>::SplineBlend(const rw::trajectory::Interpolator<double>& interp1,
                                     const rw::trajectory::Interpolator<double>& interp2,
                                     double tau1, double tau2) :
        _tau1(tau1),
        _tau2(tau2) {
        if(tau1 > interp1.duration() || tau1 < 0) { RW_THROW("tau1 is out of bounds"); }
        if(tau2 > interp2.duration() || tau2 < 0) { RW_THROW("tau2 is out of bounds"); }

        double x0 = interp1.x(interp1.duration() - tau1);
        double x1 = interp2.x(tau2);

        double v0 = interp1.dx(interp1.duration() - tau1);
        double v1 = interp2.dx(tau2);

        _a = x0;
        _b = v0;

        double t1 = tau1 + tau2;

        _d = (2 * _a + _b * t1 + t1 * v1 - 2 * x1) / (t1 * t1 * t1);
        _c = -(3 * _a + 2 * _b * t1 + t1 * v1 - 3 * x1) / (t1 * t1);
    }

    template<> double SplineBlend<double>::x(double t) const {
        return _a + _b * t + _c * t * t + _d * t * t * t;
    }

    template<> double SplineBlend<double>::dx(double t) const {
        return _b + 2 * _c * t + 3 * _d * t * t;
    }

    template<> double SplineBlend<double>::ddx(double t) const {
        return 2 * _c + 6 * _d * t;
    }

    template<> double SplineBlend<double>::maxDdx() const {
        double a0 = std::abs(this->ddx(0));
        double a1 = std::abs(this->ddx(_tau1 + _tau2));
        if(a0 > a1) { return a0; }
        return a1;
    }

    template class SplineBlend<double>;

}}    // namespace rw::trajectory