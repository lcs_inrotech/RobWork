#include <rw/math/Q.hpp>
#include <rw/math/Vector2D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/trajectory/LimiterLinearInterpolator.hpp>

using namespace rw::trajectory;
using namespace rw::math;

namespace {
bool sign(double x) {
    return x < 0;
}

double LimiterLinearDuration(double x0, double x, double v0, double& v, double& a,
                             double& switchTime) {
    // Find Move Direction
    double moveDirection = x - x0;

    // Modify velocity direction if needed
    if(moveDirection < 0 && v > 0) { v = -v; }
    else if(moveDirection > 0 && v < 0) { v = -v; }

    // calculate how long it will take to reach max velocity
    switchTime = (v - v0) / a;

    // if the time is negative, we need to switch direction
    if(switchTime < 0) {
        a          = -a;
        switchTime = (v - v0) / a;
    }

    // calculate how long it will take to reach the end position, under constant
    // acceleration
    double endTime_constAccel1 = -((sqrt(2 * a * (x - x0) + v0 * v0) + v0) / a);
    double endTime_constAccel2 = ((sqrt(2 * a * (x - x0) + v0 * v0) - v0) / a);

    double endTime_contAccel = 0;
    if(endTime_constAccel1 > 0) { endTime_contAccel = endTime_constAccel1; }
    else if(endTime_constAccel2 > 0) { endTime_contAccel = endTime_constAccel2; }
    else if(std::isnan(endTime_constAccel1) || std::isnan(endTime_constAccel2)) { return -1; }

    // if the time to reach the end position is less than the time to reach max velocity,
    // and above 0. The whole motion is under constant acceleration, and the duration is
    // there for the time to reach the end position under acceleration
    if(endTime_contAccel > 0 && endTime_contAccel < switchTime) { return endTime_contAccel; }
    // else the duration is the time is the switch time + the time for linear motion to the
    // end
    else {
        return switchTime + (x - (x0 + v0 * switchTime + 0.5 * a * switchTime * switchTime)) / v;
    }
    return -1;
}

bool LimiterLinearSetDuration(double x0, double x, double v0, double& v, double& a,
                              double& switchTime, double d) {
    double ao  = a;
    double vo  = v;
    a          = (v - v0) * (v - v0) / (2 * (d * v - x + x0));
    switchTime = (v - v0) / a;

    if(switchTime > d) {
        a = -(2 * (d * v0 - x + x0)) / (d * d);
        v = -(d * v0 - 2 * x + 2 * x0) / d;

        switchTime = d;
    }
    // If Sign change the solution dosn't work
    if(sign(a) != sign(ao) || sign(v) != sign(vo)) {
        return false;
    }
    // If the solution is close to the old solution, we are good
    if(std::abs(a - ao) < 10e-10 && std::abs(v - vo) < 10e-10) { return true; }
    // if the new solution is larger than the old solution, something went wrong
    if(std::abs(a) > std::abs(ao) || std::abs(v) > std::abs(vo)) {
        return false;
    }

    return true;
}
}    // namespace

template<class T>
LimiterLinearInterpolator<T>::LimiterLinearInterpolator(const T& start, const T& end,
                                                        const T& startVelocity,
                                                        const T& maxVelocity,
                                                        const T& maxAcceleration) :
    _x0(start),
    _x(end), _v0(startVelocity), _v(maxVelocity), _a(maxAcceleration), _switchTime(start),
    _switchPosition(end) {
    double maxDuration = -1;
    T duration(_switchTime);
    for(size_t i = 0; i < start.size(); i++) {
        duration[i] = LimiterLinearDuration(_x0[i], _x[i], _v0[i], _v[i], _a[i], _switchTime[i]);

        if(duration[i] > maxDuration) { maxDuration = duration[i]; }
    }

    if(maxDuration < 0) { RW_THROW("No valid duration found"); }
    _duration = maxDuration;

    for(size_t i = 0; i < start.size(); i++) {
        bool success = LimiterLinearSetDuration(
            _x0[i], _x[i], _v0[i], _v[i], _a[i], _switchTime[i], _duration);
        _switchPosition[i] =
            _x0[i] + _v0[i] * _switchTime[i] + 0.5 * _a[i] * _switchTime[i] * _switchTime[i];

        if(!success) { RW_THROW("No acceleration found"); }
    }
}

template<>
LimiterLinearInterpolator<double>::LimiterLinearInterpolator(const double& start, const double& end,
                                                             const double& startVelocity,
                                                             const double& maxVelocity,
                                                             const double& maxAcceleration) :
    _x0(start),
    _x(end), _v0(startVelocity), _v(maxVelocity), _a(maxAcceleration) {
    // Find Move Direction
    double moveDirection = _x - _x0;

    // Modify velocity direction if needed
    if(moveDirection < 0 && _v > 0) { _v = -_v; }
    else if(moveDirection > 0 && _v < 0) { _v = -_v; }

    // calculate how long it will take to reach max velocity
    _switchTime = (_v - _v0) / _a;

    // if the time is negative, we need to switch direction
    if(_switchTime < 0) {
        _a          = -_a;
        _switchTime = (_v - _v0) / _a;
    }

    // calculate how long it will take to reach the end position, under constant
    // acceleration
    double endTime_constAccel1 = -((sqrt(2 * _a * (_x - _x0) + _v0 * _v0) + _v0) / _a);
    double endTime_constAccel2 = ((sqrt(2 * _a * (_x - _x0) + _v0 * _v0) - _v0) / _a);

    double endTime_contAccel = 0;
    if(endTime_constAccel1 > 0) { endTime_contAccel = endTime_constAccel1; }
    else if(endTime_constAccel2 > 0) { endTime_contAccel = endTime_constAccel2; }
    else if(std::isnan(endTime_constAccel1) || std::isnan(endTime_constAccel2)) {
        endTime_contAccel = -1;
    }

    // if the time to reach the end position is less than the time to reach max velocity,
    // and above 0. The whole motion is under constant acceleration, and the duration is
    // there for the time to reach the end position under acceleration
    if(endTime_contAccel > 0 && endTime_contAccel < _switchTime) {
        _switchPosition = end;
        _duration       = endTime_contAccel;
    }
    // else the duration is the time is the switch time + the time for linear motion to the
    // end
    else {
        _switchPosition = _x0 + _v0 * _switchTime + 0.5 * _a * _switchTime * _switchTime;

        _duration = _switchTime + (_x - _switchPosition) / _v;
    }
}

template LimiterLinearInterpolator<Q>::LimiterLinearInterpolator(const Q& start, const Q& end,
                                                                 const Q& startVelocity,
                                                                 const Q& maxVelocity,
                                                                 const Q& maxAcceleration);
template LimiterLinearInterpolator<Vector3D<double>>::LimiterLinearInterpolator(
    const Vector3D<double>& start, const Vector3D<double>& end,
    const Vector3D<double>& startVelocity, const Vector3D<double>& maxVelocity,
    const Vector3D<double>& maxAcceleration);

template LimiterLinearInterpolator<Vector2D<double>>::LimiterLinearInterpolator(
    const Vector2D<double>& start, const Vector2D<double>& end,
    const Vector2D<double>& startVelocity, const Vector2D<double>& maxVelocity,
    const Vector2D<double>& maxAcceleration);

template<class T> T LimiterLinearInterpolator<T>::x(double t) const {
    T res = _x0;
    for(size_t i = 0; i < res.size(); i++) {
        if(t < _switchTime[i]) { res[i] = _x0[i] + _v0[i] * t + 0.5 * _a[i] * t * t; }
        else { res[i] = _v[i] * (t - _switchTime[i]) + _switchPosition[i]; }
    }
    return res;
}

template<> double LimiterLinearInterpolator<double>::x(double t) const {
    if(t < _switchTime) { return _x0 + _v0 * t + 0.5 * _a * t * t; }
    t -= _switchTime;
    return _v * t + _switchPosition;
}

template<> float LimiterLinearInterpolator<float>::x(double t) const {
    if(t < _switchTime) { return _x0 + _v0 * t + 0.5 * _a * t * t; }
    t -= _switchTime;
    return _v * t + _switchPosition;
}

template Q LimiterLinearInterpolator<Q>::x(double t) const;
template Vector3D<double> LimiterLinearInterpolator<Vector3D<double>>::x(double t) const;
template Vector2D<double> LimiterLinearInterpolator<Vector2D<double>>::x(double t) const;

template<class T> T LimiterLinearInterpolator<T>::dx(double t) const {
    T res = _x0;
    for(size_t i = 0; i < res.size(); i++) {
        if(t < _switchTime[i]) { res[i] = _a[i] * t + _v0[i]; }
        else { res[i] = _v[i]; }
    }
    return res;
}

template<> double LimiterLinearInterpolator<double>::dx(double t) const {
    if(t < _switchTime) { return _a * t + _v0; }
    t -= _switchTime;

    return _v;
};

template<> float LimiterLinearInterpolator<float>::dx(double t) const {
    if(t < _switchTime) { return _a * t + _v0; }
    t -= _switchTime;

    return _v;
};

template Q LimiterLinearInterpolator<Q>::dx(double t) const;
template Vector3D<double> LimiterLinearInterpolator<Vector3D<double>>::dx(double t) const;
template Vector2D<double> LimiterLinearInterpolator<Vector2D<double>>::dx(double t) const;

template<> double LimiterLinearInterpolator<double>::ddx(double t) const {
    if(t < _switchTime) { return _a; }
    return 0;
}
template<> float LimiterLinearInterpolator<float>::ddx(double t) const {
    if(t < _switchTime) { return _a; }
    return 0;
}

template<class T> T LimiterLinearInterpolator<T>::ddx(double t) const {
    T res = _a;
    for(size_t i = 0; i < res.size(); i++) {
        if(t >= _switchTime[i]) { res[i] = 0; }
    }
    return res;
}

template Q LimiterLinearInterpolator<Q>::ddx(double t) const;
template Vector3D<double> LimiterLinearInterpolator<Vector3D<double>>::ddx(double t) const;
template Vector2D<double> LimiterLinearInterpolator<Vector2D<double>>::ddx(double t) const;
