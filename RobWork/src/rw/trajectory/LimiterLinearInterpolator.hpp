#include <rw/core/macros.hpp>
#include <rw/math/Q.hpp>
#include <rw/math/Vector2D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/trajectory/Interpolator.hpp>

#include <cmath>
#include <iostream>
namespace rw { namespace trajectory {

    /**
     * @brief This class implements an interpolator that will move from a start position to an end
     * position, as fast as possible given a maximum velocity and acceleration. For Types with
     * multiple dimensions, the slowest dimension will set a limit for the whole motion.
     *
     * @tparam T The type of the position, velocity and acceleration. Currently implemented types
     * are double, rw::math::Q, rw::math::Vector2D and rw::math::Vector3D
     */
    template<class T> class LimiterLinearInterpolator : public rw::trajectory::Interpolator<T>
    {
      public:
        //! @brief smart pointer type to instance of class
        typedef typename rw::core::Ptr<LimiterLinearInterpolator> Ptr;

        //! @brief smart pointer type const instance of class
        typedef typename rw::core::Ptr<const LimiterLinearInterpolator> CPtr;

        /**
         * @brief This will interpolate a path starting with linear acceleration, and then
         * transition to linear motion. 
         * Meaning X = X0 + V0*t + 0.5*a*t^2 for t < switchTime 
         * and X = V*t + X1 after the switch time.
         *
         * @param start [in] start position
         * @param end [in] end position
         * @param startVelocity [in] start velocity
         * @param maxVelocity [in] maximum velocity
         * @param maxAcceleration [in] maximum acceleration
         * @throw rw::common::Exception is thrown the calculated duration is < 0
         * @throw rw::common::Exception is thrown  if one of the dimensions can't calculate a valid
         * parameter set that can keep it in continues motion while the slowest dimension is moving
         */
        LimiterLinearInterpolator(const T& start, const T& end, const T& startVelocity,
                                  const T& maxVelocity, const T& maxAcceleration);

        /**
         * @copydoc rw::trajectory::Interpolator::x
         */
        T x(double t) const;

        /**
         * @copydoc rw::trajectory::Interpolator::dx
         */
        T dx(double t) const;

        /**
         * @copydoc rw::trajectory::Interpolator::ddx
         */
        T ddx(double t) const;

        /**
         * @brief Returns the start position of the interpolator
         * @return The start position of the interpolator
         */
        T getStart() const { return _x0; }

        /**
         * @brief Returns the end position of the interpolator
         * @return The end position of the interpolator
         */
        T getEnd() const { return this->x(_duration); }

        /**
         * @copydoc rw::trajectory::Interpolator::duration
         */
        double duration() const { return _duration; }

        T switchTime() const { return _switchTime; }

      private:
        T _x0;
        T _x;
        T _v0;
        T _v;
        T _a;

        T _switchTime;
        T _switchPosition;
        double _duration;
    };
}}    // namespace rw::trajectory