/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute, 
 * Faculty of Engineering, University of Southern Denmark 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/


#ifndef RW_GEOMETRY_CUTCYLINDER_HPP
#define RW_GEOMETRY_CUTCYLINDER_HPP

#include "Primitive.hpp"
#include <rw/math/Transform3D.hpp>

namespace rw {
namespace geometry {
//! @addtogroup geometry
// @{

	/**
	 * @brief a cylinder primitive with angled end plates. By default the radius is in the x-y plane and height is along the z-axis. Shorter side towards X.
	 */
	class CutCylinder: public Primitive {
	public:

		/**
		 * @brief constructor
		 */
		CutCylinder(int levels=16);

		/**
		 * @brief Constructs cut cylinder primitive with the specified setup
		 *
		 * The cylinder is aligned with the height in the z-direction with angled end plates. Shorter side towards X.
		 *
		 * @param radius [in] radius of the cylinder.
		 * @param height [in] height of the cylinder.
		 * @param angle [in] angle of the cut at the end plate of cylinder. Shorter side towards X.
     * @param levels [in] granularity of the mesh
		 */
		CutCylinder(float radius, float height, float angle,int levels=16);
		
		/**
		 * @brief Constructor.
		 * @param initQ [in] vector with (height, radius)
     * @param levels [in] granularity of the mesh
		 */
		CutCylinder(const rw::math::Q& initQ,int levels=16);

		/**
		 * @brief Construct cut cylinder primitive with specified radius, height, angle and with the given transform.
		 *
		 * The cylinder will be centered in the position of \b transform and oriented in the direction of the third column of the 
		 * rotation matrix of \b transform.
		 * @param transform [in] The transform specifying how the pose of the cylinder
		 * @param radius [in] radius of the cylinder.
		 * @param height [in] height of the cylinder.
		 * @param angle [in] angle of the cut at the end plate of cylinder. Shorter side towards X.
     * @param levels [in] granularity of the mesh
		 */
		CutCylinder(const rw::math::Transform3D<>& transform, float radius, float height, float angle, int levels=16);

		//! @brief destructor
		virtual ~CutCylinder();

		/**
		 * @brief Get the radius of the cylinder.
		 * @return the radius.
		 */
		double getRadius() const { return _radius;}

		/**
		 * @brief Get the height of the cylinder.
		 * @return the height.
		 */
		double getHeight() const { return _height;}

		/**
		 * @brief Get the angle of the cut at the ends. Shorter side towards X.
		 * @return the angle.
		 */
		double getAngle() const { return _angle; }

		/**
		 * @brief Returns the transform of the cylinder.
		 * 
		 * Default is the identity matrix unless a transform has been specified.
		 * @return Transform of the cylinder
		 */
		const rw::math::Transform3D<float>& getTransform() const { return _transform; }

		// inherited from Primitive

		//! @copydoc Primitive::createMesh
		TriMesh::Ptr createMesh(int resolution) const;

		//! @copydoc Primitive::getParameters
		virtual rw::math::Q getParameters() const;
		
		//! @copydoc Primitive::setParameters
		virtual void setParameters(const rw::math::Q& q);

		//! @copydoc GeometryData::getType
		GeometryType getType() const { return CylinderPrim; };

	private:
		rw::math::Transform3D<float> _transform;
		float _radius;
		float _height;
		float _angle;
	};
	//! @}

} // geometry
} // rw

#endif /* CYLINDER_HPP_ */
