/********************************************************************************
 * Copyright 2013 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#include "ThreadPool.hpp"

#include "ThreadSafeVariable.hpp"

#include <rw/core/macros.hpp>

#include <boost/bind/bind.hpp>
#include <boost/function.hpp>
#include <boost/thread/thread.hpp>
#include <boost/version.hpp>

#if(BOOST_VERSION < 106600)
#include <boost/asio/io_service.hpp>
#else
#include <boost/asio/io_context.hpp>
#include <boost/asio/post.hpp>
#endif

using namespace rw::common;

struct ThreadPool::Internals
{
#if(BOOST_VERSION < 106600)
    Internals() : _work(new boost::asio::io_service::work(_service)) {}
    boost::asio::io_service _service;
    boost::asio::io_service::work* _work;
#else
    Internals() :
        _work(new boost::asio::executor_work_guard<boost::asio::io_context::executor_type>(
            boost::asio::make_work_guard(_service))) {}
    boost::asio::io_context _service;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type>* _work;
#endif
};

ThreadPool::ThreadPool(int threads) :
    _internals(new Internals()),
    _threadsNumber(threads < 0 ? boost::thread::hardware_concurrency() : threads),
    _postStop(new ThreadSafeVariable<bool>(false)),
    _queueSize(new ThreadSafeVariable<unsigned int>(0)) {
    for(unsigned int i = 0; i < _threadsNumber; i++) {
#if(BOOST_VERSION < 106600)
        boost::function<std::size_t()> fct = boost::bind(
            static_cast<std::size_t (boost::asio::io_service::*)()>(&boost::asio::io_service::run),
            &_internals->_service);
#else
        boost::function<std::size_t()> fct = boost::bind(
            static_cast<std::size_t (boost::asio::io_context::*)()>(&boost::asio::io_context::run),
            &_internals->_service);
#endif
        _threads.create_thread(fct);
    }
}

ThreadPool::~ThreadPool() {
    stop();
    delete _postStop;
    delete _queueSize;
    delete _internals;
}

unsigned int ThreadPool::getNumberOfThreads() const {
    return _threadsNumber;
}

void ThreadPool::stop() {
    _postStop->setVariable(true);
    _internals->_service.stop();
    delete _internals->_work;
    _internals->_work = nullptr;
    _threads.interrupt_all();
    _threads.join_all();
    _queueSize->setVariable(0);
}

bool ThreadPool::isStopping() {
    return _postStop->getVariable();
}

void ThreadPool::addWork(WorkFunction work) {
    {
        boost::mutex::scoped_lock lock(_queueSizeMutex);
        _queueSize->setVariable(_queueSize->getVariable() + 1);
    }
    if(_threads.size() == 0) { runWrap(work); }
    else {
#if(BOOST_VERSION < 106600)
        _internals->_service.post(boost::bind(&ThreadPool::runWrap, this, work));
#else
        boost::asio::post(_internals->_service, boost::bind(&ThreadPool::runWrap, this, work));
#endif
    }
}

unsigned int ThreadPool::getQueueSize() {
    return _queueSize->getVariable();
}

void ThreadPool::waitForEmptyQueue() {
    unsigned int var = _queueSize->getVariable();
    while(var != 0) { var = _queueSize->waitForUpdate(var); }
}

void ThreadPool::runWrap(WorkFunction work) {
    try {
        work(this);
    }
    catch(boost::thread_interrupted const&) {
        RW_THROW("Please catch boost::thread_interrupted exception thrown in work function for "
                 "ThreadPool!");
    }
    {
        boost::mutex::scoped_lock lock(_queueSizeMutex);
        _queueSize->setVariable(_queueSize->getVariable() - 1);
    }
}
