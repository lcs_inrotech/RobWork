/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#ifndef RW_COMMON_TYPES_HPP
#define RW_COMMON_TYPES_HPP

#include <rw/core/os.hpp>

#include <exception>
#include <limits>
#include <stdexcept>
#include <stdint.h>

namespace rw { namespace common {
    template<typename Dst, typename Src> inline Dst numeric_cast(Src value) {
        typedef std::numeric_limits<Dst> DstLim;
        typedef std::numeric_limits<Src> SrcLim;

#if defined(RW_LINUX) && !defined(RW_MACOS)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#endif

        const bool positive_overflow_possible = DstLim::max() < SrcLim::max();
        const bool negative_overflow_possible =
            SrcLim::is_signed || (DstLim::lowest() > SrcLim::lowest());

#if defined(RW_LINUX) && !defined(RW_MACOS)
#pragma GCC diagnostic pop
#endif

        // unsigned <-- unsigned
        if((!DstLim::is_signed) && (!SrcLim::is_signed)) {
            if(positive_overflow_possible && (value > DstLim::max())) {
                throw std::overflow_error(
                    "During conversion from unsigned -> unsigned: positive overflow");
            }
        }
        // unsigned <-- signed
        else if((!DstLim::is_signed) && SrcLim::is_signed) {
            if(positive_overflow_possible && (value > DstLim::max())) {
                throw std::overflow_error(
                    "During conversion from signed -> unsigned: positive overflow");
            }
            else if(negative_overflow_possible && (value < 0)) {
                throw std::overflow_error(
                    "During conversion from signed -> unsigned: negative overflow");
            }
        }
        // signed <-- unsigned
        else if(DstLim::is_signed && (!SrcLim::is_signed)) {
            if(positive_overflow_possible && (value > DstLim::max())) {
                throw std::overflow_error(
                    "During conversion from unsigned -> signed: positive overflow");
            }
        }
        // signed <-- signed
        else if(DstLim::is_signed && SrcLim::is_signed) {
            if(positive_overflow_possible && (value > DstLim::max())) {
                throw std::overflow_error(
                    "During conversion from signed -> signed: positive overflow");
            }
            else if(negative_overflow_possible && (value < DstLim::lowest())) {
                throw std::overflow_error(
                    "During conversion from signed -> signed: negative overflow");
            }
        }

        // limits have been checked, therefore safe to cast
        return static_cast<Dst>(value);
    }
}}    // namespace rw::common

#endif