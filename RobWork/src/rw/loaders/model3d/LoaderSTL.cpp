#include <rw/loaders/model3d/LoaderSTL.hpp>

#include <filesystem>

using namespace rw::loaders;
using namespace rw::geometry;
using namespace rw::core;

rw::graphics::Model3D::Ptr LoaderSTL::load(const std::string& filename) {
    std::string name = _defaultName;
    if(name.empty()) { name = std::filesystem::path(filename).filename().string(); }

    rw::geometry::PlainTriMeshN1F::Ptr mesh = STLFile::load(filename);
    Model3D::Ptr model                      = rw::core::ownedPtr(new Model3D(name));

    model->addTriMesh(_defaultMat, *mesh);

    return model;
}
