/*
 * ParallelAxisDHCalibration.cpp
 */

#include "ParallelAxisDHCalibration.hpp"

#include <rw/core/Log.hpp>
#include <rw/core/macros.hpp>
#include <rw/models/DHParameterSet.hpp>

using namespace rw::math;

namespace rwlibs { namespace calibration {

    ParallelAxisDHCalibration::ParallelAxisDHCalibration (rw::models::Joint::Ptr joint) :
        _parameterSet (6), _joint (joint),
        _originalSet (*rw::models::DHParameterSet::get (joint.get ()))
    {
        _originalTransform = _joint->getFixedTransform ();
    }

    ParallelAxisDHCalibration::~ParallelAxisDHCalibration ()
    {}

    rw::models::Joint::Ptr ParallelAxisDHCalibration::getJoint () const
    {
        return _joint;
    }

    void ParallelAxisDHCalibration::doApply ()
    {
        CalibrationParameterSet parameterSet = getParameterSet ();
        const double a =
            parameterSet (PARAMETER_A).isEnabled () ? parameterSet (PARAMETER_A).getValue () : 0;
        const double alpha = parameterSet (PARAMETER_ALPHA).isEnabled ()
                                 ? parameterSet (PARAMETER_ALPHA).getValue ()
                                 : 0;
        const double b =
            parameterSet (PARAMETER_B).isEnabled () ? parameterSet (PARAMETER_B).getValue () : 0;
        const double beta = parameterSet (PARAMETER_BETA).isEnabled ()
                                ? parameterSet (PARAMETER_BETA).getValue ()
                                : 0;
        const double d =
            parameterSet (PARAMETER_D).isEnabled () ? parameterSet (PARAMETER_D).getValue () : 0;
        const double theta = parameterSet (PARAMETER_THETA).isEnabled ()
                                 ? parameterSet (PARAMETER_THETA).getValue ()
                                 : 0;

        rw::math::Transform3D<> correctedTransform;
        if (parameterSet (PARAMETER_B).isEnabled () || parameterSet (PARAMETER_BETA).isEnabled ()) {
            rw::core::Log::log ().debug () << "DHHGP, alpha: " << alpha << ", a: " << a
                << ", beta: " << beta << ", b: " << b << std::endl;
            correctedTransform =
                _originalTransform * rw::math::Transform3D< double >::DHHGP (alpha, a, beta, b);
        }
        else if (parameterSet (PARAMETER_D).isEnabled () ||
                 parameterSet (PARAMETER_THETA).isEnabled ()) {
            rw::core::Log::log ().debug () << "DH, alpha: " << alpha << ", a: " << a
                << ", d: " << d << ", theta: " << theta << std::endl;
            correctedTransform =
                rw::math::Transform3D< double >::DH (_originalSet.alpha () + alpha,
                                                     _originalSet.a () + a,
                                                     _originalSet.d () + d,
                                                     _originalSet.theta () + theta);
        }
        else {
            RW_THROW ("Unknown DH parameter set.");
        }

        _joint->setFixedTransform (correctedTransform);
    }

    void ParallelAxisDHCalibration::doRevert ()
    {
        rw::models::DHParameterSet::set (rw::models::DHParameterSet (0, 0, 0, 0), _joint.get ());
        // const rw::math::Transform3D<> correctedTransform = Transform3D<>::identity();
        _joint->setFixedTransform (_originalTransform);
    }

    rw::math::Transform3D<>
    ParallelAxisDHCalibration::computeTransform (const rw::models::DHParameterSet& dhParameterSet)
    {
        return rw::math::Transform3D< double >::DHHGP (dhParameterSet.alpha (),
                                                       dhParameterSet.a (),
                                                       dhParameterSet.beta (),
                                                       dhParameterSet.b ());
    }

    CalibrationParameterSet ParallelAxisDHCalibration::getParameterSet () const
    {
        return _parameterSet;
    }

    void ParallelAxisDHCalibration::setParameterSet (const CalibrationParameterSet& parameterSet)
    {
        _parameterSet = parameterSet;
    }
}}    // namespace rwlibs::calibration
