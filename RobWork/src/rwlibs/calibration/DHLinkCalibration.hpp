/*
 * DHLinkCalibration.hpp
 *
 *  Created on: Aug 28, 2012
 *      Author: bing
 */

#ifndef RWLIBS_CALIBRATION_DHLINKCALIBRATION_HPP
#define RWLIBS_CALIBRATION_DHLINKCALIBRATION_HPP

#include "Calibration.hpp"
#include "CalibrationParameterSet.hpp"

#include <rw/models/DHParameterSet.hpp>
#include <rw/models/Joint.hpp>

namespace rwlibs { namespace calibration {

    class DHLinkCalibration : public Calibration
    {
      public:
        enum {
            PARAMETER_A = 0,
            PARAMETER_B,
            PARAMETER_D,
            PARAMETER_ALPHA,
            PARAMETER_BETA,
            PARAMETER_THETA
        };

        typedef rw::core::Ptr< DHLinkCalibration > Ptr;

        typedef rw::core::Ptr< const DHLinkCalibration > CPtr;

        DHLinkCalibration (rw::models::Joint::Ptr joint);

        virtual ~DHLinkCalibration ();

        rw::models::Joint::Ptr getJoint () const;

        /**
         * @brief Returns the parameter set for the calibration
         */
        CalibrationParameterSet getParameterSet () const;

        /**
         * @brief Sets the parameter set for the calibration
         */
        void setParameterSet (const CalibrationParameterSet& parameterSet);

      private:
        virtual void doApply ();

        virtual void doRevert ();

        static rw::math::Transform3D<>
        computeTransform (const rw::models::DHParameterSet& dhParameterSet);

      private:
        CalibrationParameterSet _parameterSet;
        rw::models::Joint::Ptr _joint;
        rw::models::DHParameterSet _originalSet;
        bool _isParallel;
    };

}}    // namespace rwlibs::calibration

#endif /* RWLIBS_CALIBRATION_DHLINKCALIBRATION_HPP_ */
