/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#include "RRTQToQPlanner.hpp"

#include "RDTBalancedBidirectional.hpp"
#include "RRTBasic.hpp"
#include "RRTConnect.hpp"

#include <rw/pathplanning/PlannerConstraint.hpp>
#include <rw/pathplanning/QSampler.hpp>

#include <algorithm>
#include <cfloat>

using namespace rw;
using namespace rw::math;
using namespace rw::core;
using namespace rw::pathplanning;
using namespace rw::trajectory;
using namespace rwlibs::pathplanners;

QToQPlanner::Ptr RRTQToQPlanner::makeBasic(const PlannerConstraint& constraint,
                                           const QSampler::Ptr& sampler,
                                           const QMetric::CPtr& metric, double extend) {
    return ownedPtr(new RRTBasic(constraint, sampler, metric, extend));
}

QToQPlanner::Ptr RRTQToQPlanner::makeConnect(const PlannerConstraint& constraint,
                                             const QSampler::Ptr& sampler,
                                             const QMetric::CPtr& metric, double extend) {
    return ownedPtr(new RRTConnect(constraint, sampler, metric, extend));
}

QToQPlanner::Ptr RRTQToQPlanner::makeBidirectional(const PlannerConstraint& constraint,
                                                   const QSampler::Ptr& sampler,
                                                   const QMetric::CPtr& metric, double extend) {
    return ownedPtr(
        new RDTBalancedBidirectional(constraint, sampler, metric, extend, true));
}

QToQPlanner::Ptr RRTQToQPlanner::makeBalancedBidirectional(const PlannerConstraint& constraint,
                                                           const QSampler::Ptr& sampler,
                                                           const QMetric::CPtr& metric,
                                                           double extend) {
    return ownedPtr(
        new RDTBalancedBidirectional(constraint, sampler, metric, extend, false));
}
