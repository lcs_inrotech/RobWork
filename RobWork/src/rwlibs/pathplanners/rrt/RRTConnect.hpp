/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#ifndef RWLIBS_PATHPLANNERS_RRT_RRTCONNECT_HPP
#define RWLIBS_PATHPLANNERS_RRT_RRTCONNECT_HPP

#include "RRTAbstraction.hpp"

#include <rw/math/Metric.hpp>
#include <rw/pathplanning/PlannerConstraint.hpp>
#include <rw/pathplanning/QSampler.hpp>
#include <rw/pathplanning/QToQPlanner.hpp>

namespace rwlibs { namespace pathplanners {

    class RRTConnect : public rw::pathplanning::QToQPlanner, public RRTAbstraction
    {
      public:
        //! @brief Smart pointer type for a RRTConnect.
        typedef rw::core::Ptr<RRTConnect> Ptr;

        //! @brief Smart pointer type for a const RRTConnect.
        typedef rw::core::Ptr<const RRTConnect> CPtr;

        RRTConnect(const rw::pathplanning::PlannerConstraint& constraint,
                   const rw::pathplanning::QSampler::Ptr& sampler,
                   const rw::math::QMetric::CPtr& metric, double extend) :
            RRTAbstraction(constraint, sampler, metric, extend) {}

      private:
        bool doQuery(const rw::math::Q& start, const rw::math::Q& goal,
                     rwlibs::pathplanners::RRTAbstraction::Path& result,
                     const rw::pathplanning::StopCriteria& stop);
    };
}}    // namespace rwlibs::pathplanners

#endif    // end include guard
