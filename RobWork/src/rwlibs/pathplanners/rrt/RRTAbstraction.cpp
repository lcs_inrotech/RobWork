/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#include "RRTAbstraction.hpp"

#include <algorithm>
#include <boost/foreach.hpp>
#include <limits>

const rw::math::Q& rwlibs::pathplanners::RRTAbstraction::getQ(Node* node) const {
    return node->getValue();
}

bool rwlibs::pathplanners::RRTAbstraction::inCollision(const rw::math::Q& q) const {
    return _constraint.getQConstraint().inCollision(q);
}

// 'node' is known to be collision free, but 'b' is not.
bool rwlibs::pathplanners::RRTAbstraction::inCollision(Node* a, const rw::math::Q& b) const {
    return _constraint.getQConstraint().inCollision(b) ||
           _constraint.getQEdgeConstraint().inCollision(getQ(a), b);
}

// Brute-force nearest neighbor search.
rwlibs::pathplanners::RRTAbstraction::Node*
rwlibs::pathplanners::RRTAbstraction::nearestNeighbor(const Tree& tree,
                                                      const rw::math::Q& q) const {
    double minLength = std::numeric_limits<double>::max();
    Node* minNode    = nullptr;

    for(Node* node : tree.getNodes()) {
        const double length = _metric->distance(q, getQ(node));
        if(length < minLength) {
            minLength = length;
            minNode   = node;
        }
    }

    RW_ASSERT(minNode);
    return minNode;
}

rwlibs::pathplanners::RRTAbstraction::ExtendResult
rwlibs::pathplanners::RRTAbstraction::extend(Tree& tree, const rw::math::Q& q,
                                             Node* qNearNode) const {
    const rw::math::Q& qNear = getQ(qNearNode);
    const rw::math::Q delta  = q - qNear;
    const double dist        = _metric->distance(delta);

    if(dist <= _extend) {
        if(!inCollision(qNearNode, q)) {
            tree.add(q, qNearNode);
            return ExtendResult::Reached;
        }
        else { return ExtendResult::Trapped; }
    }
    else {
        const rw::math::Q qNew = qNear + (_extend / dist) * delta;
        if(!inCollision(qNearNode, qNew)) {
            tree.add(qNew, qNearNode);
            return ExtendResult::Advanced;
        }
        else { return ExtendResult::Trapped; }
    }
}

rwlibs::pathplanners::RRTAbstraction::ExtendResult
rwlibs::pathplanners::RRTAbstraction::connect(Tree& tree, const rw::math::Q& q) const {
    Node* qNearNode = nearestNeighbor(tree, q);

    ExtendResult s   = ExtendResult::Advanced;
    bool hasAdvanced = false;
    while(s == ExtendResult::Advanced) {
        s = extend(tree, q, qNearNode);
        if(s == ExtendResult::Advanced) {
            qNearNode   = &tree.getLast();
            hasAdvanced = true;
        }
    }

    if(s == ExtendResult::Trapped && hasAdvanced) return ExtendResult::Advanced;
    else return s;
}

rwlibs::pathplanners::RRTAbstraction::ExtendResult
rwlibs::pathplanners::RRTAbstraction::growTree(Tree& tree, const rw::math::Q& q) const {
    Node* qNearNode = nearestNeighbor(tree, q);
    return extend(tree, q, qNearNode);
}

// Assuming that both trees have just been extended, retrieve the resulting
// path.
void rwlibs::pathplanners::RRTAbstraction::getPath(const Tree& startTree, const Tree& goalTree,
                                                   Path& result) const {
    Path revPart;
    Tree::getRootPath(*startTree.getLast().getParent(), revPart);
    result.insert(result.end(), revPart.rbegin(), revPart.rend());
    Tree::getRootPath(goalTree.getLast(), result);
}

void rwlibs::pathplanners::RRTAbstraction::setSampler(
    const rw::pathplanning::QSampler::Ptr& sampler) {
    RW_ASSERT(sampler);
    std::lock_guard<std::mutex> lock(_plannerMutex);
    _sampler = sampler;
}

void rwlibs::pathplanners::RRTAbstraction::setPlannerConstraint(
    const rw::pathplanning::PlannerConstraint& constraint) {
    std::lock_guard<std::mutex> lock(_plannerMutex);
    _constraint = constraint;
}

void rwlibs::pathplanners::RRTAbstraction::setMetric(const rw::math::QMetric::CPtr& metric) {
    RW_ASSERT(metric);
    std::lock_guard<std::mutex> lock(_plannerMutex);
    _metric = metric;
}

rw::pathplanning::QSampler::Ptr rwlibs::pathplanners::RRTAbstraction::getSampler() const {
    std::lock_guard<std::mutex> lock(_plannerMutex);
    return _sampler;
}

const rw::pathplanning::PlannerConstraint
rwlibs::pathplanners::RRTAbstraction::getPlannerConstraint() const {
    std::lock_guard<std::mutex> lock(_plannerMutex);
    return _constraint;
}

rw::math::QMetric::CPtr rwlibs::pathplanners::RRTAbstraction::getMetric() const {
    std::lock_guard<std::mutex> lock(_plannerMutex);
    return _metric;
}
