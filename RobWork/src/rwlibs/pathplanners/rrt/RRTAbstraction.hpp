/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#ifndef RWLIBS_PATHPLANNERS_RRT_RRTABSTRACTION_HPP
#define RWLIBS_PATHPLANNERS_RRT_RRTABSTRACTION_HPP

/**
@file RRTAbstraction.hpp
*/

#include "RRTTree.hpp"

#include <rw/math/Metric.hpp>
#include <rw/math/Q.hpp>
#include <rw/pathplanning/PlannerConstraint.hpp>
#include <rw/pathplanning/QSampler.hpp>
#include <rw/trajectory/Path.hpp>

#include <mutex>

namespace rwlibs { namespace pathplanners {

    class RRTAbstraction
    {
      public:
        //! @brief Smart pointer type for a RRTAbstraction.
        typedef rw::core::Ptr<RRTAbstraction> Ptr;

        //! @brief Smart pointer type for a const RRTAbstraction.
        typedef rw::core::Ptr<const RRTAbstraction> CPtr;

        enum class ExtendResult { Trapped, Reached, Advanced };

        typedef RRTNode<rw::math::Q> Node;
        typedef RRTTree<rw::math::Q> Tree;
        typedef rw::trajectory::QPath Path;

        RRTAbstraction(const rw::pathplanning::PlannerConstraint& constraint,
                       const rw::pathplanning::QSampler::Ptr& sampler,
                       const rw::math::QMetric::CPtr& metric, double extend) :
            _constraint(constraint),
            _sampler(sampler), _metric(metric), _extend(extend) {
            RW_ASSERT(sampler);
            RW_ASSERT(metric);
        }

        const rw::math::Q& getQ(Node* node) const;

        bool inCollision(const rw::math::Q& q) const;

        // 'node' is known to be collision free, but 'b' is not.
        bool inCollision(Node* a, const rw::math::Q& b) const;

        // Brute-force nearest neighbor search.
        Node* nearestNeighbor(const Tree& tree, const rw::math::Q& q) const;

        ExtendResult extend(Tree& tree, const rw::math::Q& q, Node* qNearNode) const;

        ExtendResult connect(Tree& tree, const rw::math::Q& q) const;

        ExtendResult growTree(Tree& tree, const rw::math::Q& q) const;

        // Assuming that both trees have just been extended, retrieve the resulting
        // path.
        void getPath(const Tree& startTree, const Tree& goalTree, Path& result) const;

        void setSampler(const rw::pathplanning::QSampler::Ptr& sampler);
        void setPlannerConstraint(const rw::pathplanning::PlannerConstraint& constraint);
        void setMetric(const rw::math::QMetric::CPtr& metric);

        rw::pathplanning::QSampler::Ptr getSampler() const;
        const rw::pathplanning::PlannerConstraint getPlannerConstraint() const;
        rw::math::QMetric::CPtr getMetric() const;

      protected:
        rw::pathplanning::PlannerConstraint _constraint;
        rw::pathplanning::QSampler::Ptr _sampler;
        rw::math::QMetric::CPtr _metric;
        double _extend;
        // Prevent changing the constraint, metric etc while planning
        mutable std::mutex _plannerMutex;
    };

    /*\}*/
}}    // namespace rwlibs::pathplanners

#endif    // end include guard
