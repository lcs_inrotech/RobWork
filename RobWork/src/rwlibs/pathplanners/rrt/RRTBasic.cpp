/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#include "RRTBasic.hpp"

#include <rw/pathplanning/PlannerConstraint.hpp>
#include <rw/pathplanning/QSampler.hpp>

bool rwlibs::pathplanners::RRTBasic::doQuery(const rw::math::Q& start, const rw::math::Q& goal,
                                             rwlibs::pathplanners::RRTAbstraction::Path& result,
                                             const rw::pathplanning::StopCriteria& stop) {
    std::lock_guard<std::mutex> lock(_plannerMutex);
    if(inCollision(start)) {
        std::cout << "Start is in collision." << std::endl;
        return false;
    }

    if(inCollision(goal)) {
        std::cout << "Goal is in collision." << std::endl;
        return false;
    }

    if(!_constraint.getQEdgeConstraint().inCollision(start, goal)) {
        result.push_back(start);
        result.push_back(goal);
        return true;
    }

    Tree startTree(start);
    Tree goalTree(goal);

    while(!stop.stop()) {
        const rw::math::Q qAttr = _sampler->sample();
        if(qAttr.empty()) RW_THROW("Sampler must always succeed.");

        // If both trees manage to connect, then return the resulting
        // path.
        if(growTree(startTree, qAttr) == ExtendResult::Reached &&
           growTree(goalTree, qAttr) == ExtendResult::Reached) {
            getPath(startTree, goalTree, result);
            return true;
        }
    }

    return false;
}
