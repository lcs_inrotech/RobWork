#include <rw/math/Q.hpp>
#include <rw/math/Random.hpp>
#include <rw/math/Vector2D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/trajectory/InterpolatorTrajectory.hpp>
#include <rw/trajectory/LimiterLinearInterpolator.hpp>
#include <rw/trajectory/SplineBlend.hpp>

#include <gtest/gtest.h>

using namespace rw::trajectory;
using namespace rw::math;

TEST(SplineBlendTest, Construction) {
    rw::core::Ptr<LimiterLinearInterpolator<double>> intp0 =
        rw::core::ownedPtr(new LimiterLinearInterpolator<double>(0.4, 3.2, 0.1, 0.6, 0.1));
    rw::core::Ptr<LimiterLinearInterpolator<double>> intp1 =
        rw::core::ownedPtr(new LimiterLinearInterpolator<double>(3.2, 1.2, 0.6, 0.4, 0.2));
    rw::core::Ptr<SplineBlend<double>> blend = rw::core::ownedPtr(
        new SplineBlend<double>(*intp0, *intp1, 0.2 * intp0->duration(), 0.2 * intp1->duration()));

    InterpolatorTrajectory<double> traj;

    traj.add(intp0);
    traj.add(blend.cast<rw::trajectory::Blend<double>>(), intp1);

    // Test the construction of SplineBlend
    ASSERT_DOUBLE_EQ(blend->tau1(), 0.2 * intp0->duration());
    ASSERT_DOUBLE_EQ(blend->tau2(), 0.2 * intp1->duration());
    ASSERT_DOUBLE_EQ(blend->x(0.0), intp0->x(intp0->duration() - blend->tau1()));
    ASSERT_DOUBLE_EQ(blend->x(blend->tau1() + blend->tau2()), intp1->x(blend->tau2()));
    ASSERT_DOUBLE_EQ(blend->dx(0.0), intp0->dx(intp0->duration() - blend->tau1()));
    ASSERT_NEAR(blend->dx(blend->tau1() + blend->tau2()), intp1->dx(blend->tau2()), 10e-15);
}

template<typename T> class SplineBlendTypeTest : public testing::Test
{};

using ArgTypes = testing::Types<Vector2D<double>, Vector3D<double>, Q>;
TYPED_TEST_CASE(SplineBlendTypeTest, ArgTypes);

TYPED_TEST(SplineBlendTypeTest, Construct) {
    rw::core::Ptr<LimiterLinearInterpolator<TypeParam>> intp0;
    rw::core::Ptr<LimiterLinearInterpolator<TypeParam>> intp1;

    bool success = false;
    while(!success) {
        try {
            TypeParam start           = rw::math::Random::ranT<TypeParam>(-4.0, 4.0, 4);
            TypeParam mid             = rw::math::Random::ranT<TypeParam>(-4.0, 4.0, 4);
            TypeParam end             = rw::math::Random::ranT<TypeParam>(-4.0, 4.0, 4);
            TypeParam startVelocity   = rw::math::Random::ranT<TypeParam>(-1.0, 1.0, 4);
            TypeParam maxVelocity     = rw::math::Random::ranT<TypeParam>(-1.0, 1.0, 4);
            TypeParam maxAcceleration = rw::math::Random::ranT<TypeParam>(-10.0, 10.0, 4);

            intp0 = rw::core::ownedPtr(new LimiterLinearInterpolator<TypeParam>(
                start, mid, startVelocity, maxVelocity, maxAcceleration));

            intp1   = rw::core::ownedPtr(new LimiterLinearInterpolator<TypeParam>(
                mid, end, intp0->x(intp0->duration()), maxVelocity, maxAcceleration));
            success = true;
        }
        catch(...) {
        }
    }

    rw::core::Ptr<SplineBlend<TypeParam>> blend = rw::core::ownedPtr(new SplineBlend<TypeParam>(
        *intp0, *intp1, 0.2 * intp0->duration(), 0.2 * intp1->duration()));

    InterpolatorTrajectory<TypeParam> traj;

    traj.add(intp0);
    traj.add(blend, intp1);

    // Test the construction of SplineBlend
    ASSERT_DOUBLE_EQ(blend->tau1(), 0.2 * intp0->duration());
    ASSERT_DOUBLE_EQ(blend->tau2(), 0.2 * intp1->duration());
    for(size_t i = 0; i < blend->x(0.0).size(); i++) {
        ASSERT_NEAR(blend->x(0.0)[i], intp0->x(intp0->duration() - blend->tau1())[i], 10e-10);
        ASSERT_NEAR(blend->x(blend->tau1() + blend->tau2())[i], intp1->x(blend->tau2())[i], 10e-10);

        ASSERT_NEAR(blend->dx(0.0)[i], intp0->dx(intp0->duration() - blend->tau1())[i], 10e-10);
        ASSERT_NEAR(
            blend->dx(blend->tau1() + blend->tau2())[i], intp1->dx(blend->tau2())[i], 10e-10);
    }
}
