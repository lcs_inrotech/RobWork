#include <rw/math/Q.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Vector2D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/trajectory/LimiterLinearInterpolator.hpp>

#include <gtest/gtest.h>

using namespace rw::trajectory;
using namespace rw::math;

TEST(LimiterLinearInterpolatorTest, Construction) {
    LimiterLinearInterpolator<double> intp(0.4, 1.4, 0.1, 0.6, 0.1);     // normal
    LimiterLinearInterpolator<double> intp1(3.2, 1.2, 0.2, 0.8, 0.2);    // negative move
    LimiterLinearInterpolator<double> intp2(
        0.3, 1.3, -0.3, 0.7, 0.3);    // negative initial velocity
    LimiterLinearInterpolator<double> intp3(0.5, 1.5, 0.4, -0.9, 0.4);    // negative max velocity
    LimiterLinearInterpolator<double> intp4(
        4.1, 1.1, 0.5, 0.5, -0.5);    // negative max acceleration
    LimiterLinearInterpolator<double> intp5(
        0.6, 1.6, 0.6, 0.4, 0.6);    // max velocity less than initial velocity
    LimiterLinearInterpolator<double> intp6(0.6, 1.6, 0.6, 0, 0.6);    // max velocity is Zero

    EXPECT_TRUE(intp.switchTime() > 0);
    EXPECT_TRUE(intp.duration() > 0);
    EXPECT_TRUE(intp1.switchTime() > 0);
    EXPECT_TRUE(intp1.duration() > 0);
    EXPECT_TRUE(intp2.switchTime() > 0);
    EXPECT_TRUE(intp2.duration() > 0);
    EXPECT_TRUE(intp3.switchTime() > 0);
    EXPECT_TRUE(intp3.duration() > 0);
    EXPECT_TRUE(intp4.switchTime() > 0);
    EXPECT_TRUE(intp4.duration() > 0);
    EXPECT_TRUE(intp5.switchTime() > 0);
    EXPECT_TRUE(intp5.duration() > 0);
    EXPECT_TRUE(intp6.switchTime() > 0);
    EXPECT_TRUE(intp6.duration() > 0);
}

namespace {
template<typename T> class LimiterLinearInterpolatorTypeTest : public testing::Test
{
  public:
    T start();
    T end();
    T initVelocity();
    T velocity();
    T acceleration();

    bool compare(const T& lhs, const T& rhs, double eps = 1e-6);
};

// Define the LimiterLinearInterpolatorTypeTest::start() function for each type
template<> double LimiterLinearInterpolatorTypeTest<double>::start() {
    return 0.4;
}
template<> Q LimiterLinearInterpolatorTypeTest<Q>::start() {
    return Q(0.4, 0.3, 0.2, 0.1);
}
template<> Vector3D<double> LimiterLinearInterpolatorTypeTest<Vector3D<double>>::start() {
    return Vector3D<double>(0.4, 0.2, 0.1);
}
template<> Vector2D<double> LimiterLinearInterpolatorTypeTest<Vector2D<double>>::start() {
    return Vector2D<double>(0.4, 0.0);
}

// Define the LimiterLinearInterpolatorTypeTest::end() function for each type
template<> double LimiterLinearInterpolatorTypeTest<double>::end() {
    return 1.4;
}
template<> Q LimiterLinearInterpolatorTypeTest<Q>::end() {
    return Q(1.4, 1.5, 1.6, 1.7);
}
template<> Vector3D<double> LimiterLinearInterpolatorTypeTest<Vector3D<double>>::end() {
    return Vector3D<double>(1.4, 1.6, 1.7);
}
template<> Vector2D<double> LimiterLinearInterpolatorTypeTest<Vector2D<double>>::end() {
    return Vector2D<double>(1.4, 1.8);
}

// Define the LimiterLinearInterpolatorTypeTest::initVelocity() function for each type
template<> double LimiterLinearInterpolatorTypeTest<double>::initVelocity() {
    return 0.0;
}
template<> Q LimiterLinearInterpolatorTypeTest<Q>::initVelocity() {
    return Q(0.0, 0.0, 0.0, 0.0);
}
template<> Vector3D<double> LimiterLinearInterpolatorTypeTest<Vector3D<double>>::initVelocity() {
    return Vector3D<double>(0.0, 0.0, 0.0);
}
template<> Vector2D<double> LimiterLinearInterpolatorTypeTest<Vector2D<double>>::initVelocity() {
    return Vector2D<double>(0.0, 0.0);
}

// Define the LimiterLinearInterpolatorTypeTest::velocity() function for each type
template<> double LimiterLinearInterpolatorTypeTest<double>::velocity() {
    return 0.1;
}
template<> Q LimiterLinearInterpolatorTypeTest<Q>::velocity() {
    return Q(0.1, 0.2, 0.3, 0.4);
}
template<> Vector3D<double> LimiterLinearInterpolatorTypeTest<Vector3D<double>>::velocity() {
    return Vector3D<double>(0.1, 0.3, 0.4);
}
template<> Vector2D<double> LimiterLinearInterpolatorTypeTest<Vector2D<double>>::velocity() {
    return Vector2D<double>(0.1, 0.5);
}

// Define the LimiterLinearInterpolatorTypeTest::acceleration() function for each type
template<> double LimiterLinearInterpolatorTypeTest<double>::acceleration() {
    return 0.6;
}
template<> Q LimiterLinearInterpolatorTypeTest<Q>::acceleration() {
    return Q(0.6, 0.7, 0.8, 0.9);
}
template<> Vector3D<double> LimiterLinearInterpolatorTypeTest<Vector3D<double>>::acceleration() {
    return Vector3D<double>(0.6, 0.8, 0.9);
}
template<> Vector2D<double> LimiterLinearInterpolatorTypeTest<Vector2D<double>>::acceleration() {
    return Vector2D<double>(0.6, 1.0);
}

// Define the LimiterLinearInterpolatorTypeTest::compare() function for each type
template<>
bool LimiterLinearInterpolatorTypeTest<double>::compare(const double& lhs, const double& rhs,
                                                        double eps) {
    return std::abs(lhs - rhs) < eps;
}
template<>
bool LimiterLinearInterpolatorTypeTest<Q>::compare(const Q& lhs, const Q& rhs, double eps) {
    bool result = (lhs - rhs).norm2() < eps;
    if(!result) {
        std::cout << "lhs: " << lhs << std::endl;
        std::cout << "rhs: " << rhs << std::endl;
        std::cout << "diff: " << (lhs - rhs).norm2() << " < " << eps << std::endl;
    }
    return result;
}
template<>
bool LimiterLinearInterpolatorTypeTest<Vector3D<double>>::compare(const Vector3D<double>& lhs,
                                                                  const Vector3D<double>& rhs,
                                                                  double eps) {
    bool result = (lhs - rhs).norm2() < eps;
    if(!result) {
        std::cout << "lhs: " << lhs << std::endl;
        std::cout << "rhs: " << rhs << std::endl;
        std::cout << "diff: " << (lhs - rhs).norm2() << " < " << eps << std::endl;
    }
    return result;
}
template<>
bool LimiterLinearInterpolatorTypeTest<Vector2D<double>>::compare(const Vector2D<double>& lhs,
                                                                  const Vector2D<double>& rhs,
                                                                  double eps) {
    bool result = (lhs - rhs).norm2() < eps;
    if(!result) {
        std::cout << "lhs: " << lhs << std::endl;
        std::cout << "rhs: " << rhs << std::endl;
        std::cout << "diff: " << (lhs - rhs).norm2() << " < " << eps << std::endl;
    }
    return result;
}

}    // namespace

using ArgTypes = testing::Types<double, Vector2D<double>, Vector3D<double>, Q>;
TYPED_TEST_CASE(LimiterLinearInterpolatorTypeTest, ArgTypes);

TYPED_TEST(LimiterLinearInterpolatorTypeTest, Construct) {
    TypeParam start        = this->start();
    TypeParam end          = this->end();
    TypeParam initVelocity = this->initVelocity();
    TypeParam velocity     = this->velocity();
    TypeParam acceleration = this->acceleration();

    LimiterLinearInterpolator<TypeParam> intp(start, end, initVelocity, velocity, acceleration);

    // Print the path as csv
    /*for(double t = 0; t < intp.duration(); t += 0.01) {
        std::cout << t << ",";
        for(size_t i = 0; i < start.size(); i++) {
            std::cout << intp.x(t)[i] << "," << intp.dx(t)[i] << "," << intp.ddx(t)[i] << ",";
        }
        std::cout << std::endl;
    }*/

    EXPECT_TRUE(this->compare(intp.x(0), start));
    EXPECT_TRUE(this->compare(intp.x(intp.duration()), end));
    EXPECT_TRUE(this->compare(intp.dx(0), initVelocity));
}