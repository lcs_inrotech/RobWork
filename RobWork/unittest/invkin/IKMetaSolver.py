#################################################################################
 # Copyright 2022 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 # Faculty of Engineering, University of Southern Denmark
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.
 #################################################################################

import unittest                                     # now we can use python unittest framework
import math 
import os
# now we import robwork python bindings
import sdurw_core                as core
import sdurw_invkin              as invkin
import sdurw_kinematics          as kinematics
import sdurw_math
import sdurw_models              as models

import numpy as np


def getKukaIIWA(stateStructure):
    base   = kinematics.ownedPtr(kinematics.FixedFrame("Base",sdurw_math.Transform3D().identity() ))
    joint1 = models.ownedPtr(models.RevoluteJoint("Joint1", sdurw_math.Transform3D(sdurw_math.Vector3D(0, 0,    0.158))))
    joint2 = models.ownedPtr(models.RevoluteJoint("Joint2", sdurw_math.Transform3D(sdurw_math.Vector3D(0, 0,    0.182), sdurw_math.RPY(0, 0, -math.pi/2))))
    joint3 = models.ownedPtr(models.RevoluteJoint("Joint3", sdurw_math.Transform3D(sdurw_math.Vector3D(0, -0.182, 0)  , sdurw_math.RPY(0, 0,  math.pi/2))))
    joint4 = models.ownedPtr(models.RevoluteJoint("Joint4", sdurw_math.Transform3D(sdurw_math.Vector3D(0, 0,    0.218), sdurw_math.RPY(0, 0,  math.pi/2))))
    joint5 = models.ownedPtr(models.RevoluteJoint("Joint5", sdurw_math.Transform3D(sdurw_math.Vector3D(0, 0.182, 0),    sdurw_math.RPY(0, 0, -math.pi/2))))
    joint6 = models.ownedPtr(models.RevoluteJoint("Joint6", sdurw_math.Transform3D(sdurw_math.Vector3D(0, 0,    0.218), sdurw_math.RPY(0, 0, -math.pi/2))))
    joint7 = models.ownedPtr(models.RevoluteJoint("Joint7", sdurw_math.Transform3D(sdurw_math.Vector3D().zero(),        sdurw_math.RPY(0, 0,  math.pi/2))))
    end    = kinematics.ownedPtr(kinematics.FixedFrame("TCP",sdurw_math.Transform3D(sdurw_math.Vector3D(0, 0, 0.126))))

    stateStructure.addFrame(base)
    stateStructure.addFrame(joint1, base)
    stateStructure.addFrame(joint2, joint1)
    stateStructure.addFrame(joint3, joint2)
    stateStructure.addFrame(joint4, joint3)
    stateStructure.addFrame(joint5, joint4)
    stateStructure.addFrame(joint6, joint5)
    stateStructure.addFrame(joint7, joint6)
    stateStructure.addFrame(end, joint7)

    state = stateStructure.getDefaultState()

    device = models.ownedPtr(models.SerialDevice(base, end, "KukaIIWA", state))

    # Python version of C++ "typedef PairQ"
    # It is defined here "~/RobWork/RobWork/src/rwlibs/swig/sdurw_math.i"
    bounds = kinematics.PairQ()
#    bounds.first = sdurw_math.Q(7, -np.deg2rad(170), -np.deg2rad(120),  -np.deg2rad(170), -np.deg2rad(120),  -np.deg2rad(170), -np.deg2rad(120), -np.deg2rad(175))
    bounds.first = sdurw_math.Q(-sdurw_math.Deg2Rad * 170, -sdurw_math.Deg2Rad * 120,  -sdurw_math.Deg2Rad * 170, -sdurw_math.Deg2Rad * 120,  -sdurw_math.Deg2Rad * 170, -sdurw_math.Deg2Rad * 120, -sdurw_math.Deg2Rad * 175)
    bounds.second = -bounds.first
    device.setBounds(bounds)

    return device


class IKMetaSolverTest(unittest.TestCase):

    def test_JacobianIKSolver(self):

        stateStructure = kinematics.StateStructure()
        state = stateStructure.getDefaultState()
        device = getKukaIIWA(stateStructure)

        jacobSolver = invkin.JacobianIKSolver(device, state)
        solver = invkin.IKMetaSolver(jacobSolver,device)

        solver.solve(device.baseTend(state), state, 50,True)
        qstart = sdurw_math.Q(0.1,  0.1, 0.1,  0.1,  0.1, 0.1,  0.1)
        print("qstart: ", qstart)
        

if __name__ == '__main__':
    unittest.main()