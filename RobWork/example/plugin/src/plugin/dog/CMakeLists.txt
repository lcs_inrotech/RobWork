add_library(Dog Dog.cpp Dog.hpp)
target_link_libraries(Dog RW::sdurw)
target_include_directories(Dog PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../..)

add_library(dog.rwplugin MODULE DogPlugin.cpp DogPlugin.hpp)
target_link_libraries(dog.rwplugin Dog RW::sdurw)
