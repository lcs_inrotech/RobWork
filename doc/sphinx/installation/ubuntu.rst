Ubuntu
**********************


RobWork can be installed in two ways:

* by PPA (Personal Package Archive),  meaning install using apt-get install
* by Source build by the user

.. toctree::
   :glob:
   :maxdepth: 1

   ubuntu_install
   ubuntu_compile

